package buscaMinas;
import java.util.*;
/**
 * <h2>Clase Jugador contiene las funciones de definir nombre de jugador y guardarlo, mostrar el ranking de puntuaci�n, y actualizar la puntuaci�n del jugador</h2>
 *
 * @version 1-2022
 * @author isanchez
 */


public class Jugador {
	static Scanner src = new Scanner(System.in);
	/**
	 * ArrayList noms guarda en cada posici�n el nombre del jugador
	 */
	static ArrayList<String> noms = new ArrayList<String>(); // guarda els noms dels jugadors
	/**
	 * ArrayList pGuanya guarda en cada posici�n correspondiente a la del jugador el
	 * n�mero de partidas ganadas
	 */
	static ArrayList<Integer> pGuanya = new ArrayList<Integer>(); // guarda el nombre de partides guanyades per cada
																	// jugador
	/**
	 * ArrayList pPerduda guarda en cada posici�n correspondiente a la del jugador
	 * el n�mero de partidas perdidas
	 */
	static ArrayList<Integer> pPerduda = new ArrayList<Integer>(); // guarda el nombre de partides perdudes per cada
																	// jugador

	/**
	 * Constructor de tipo String nombre() se encarga de coger los par�metros
	 * pasados y a�adir el nombre del jugador en el arrayList en caso de que no lo
	 * contenga, y la puntuaci�n a 0
	 * @return nombre I retorna el nom del jugador
	 */
	public static String nombre() {
		System.out.println("Definir jugador");
		String nombre = null;
		System.out.print("Escriu el nom del jugador: ");
		nombre = src.nextLine();
		if (!noms.contains(nombre)) {
			noms.add(nombre);
			pGuanya.add(0);
			pPerduda.add(0);
		}
		return nombre;
	}

	/**
	 * Constructor de tipo String showPlayer() se encarga de mostrar
	 * correlativamente en base a partidas ganadas el ranking
	 * @param player No se requiere para nada, dado que no se devuelve ni se utiliza
	 */
	public static void showPlayer(String player) {
		// TODO Auto-generated method stub
		int[] pOrdent = new int[pGuanya.size()];
		for (int i = 0; i < pOrdent.length; i++) {
			pOrdent[i] = pGuanya.get(i);

		}
		System.out.println("Jugador    ||    P.Guanyades    ||    P.Perdides");
		System.out.println("------------------------------------------------");
		for (int a = 0; a < pOrdent.length; a++) {
			int MAX = 0;
			int MIN = 9999;
			int POS = 0;
			for (int i = 0; i < pOrdent.length; i++) {
				if (pOrdent[i] > MAX) {
					MAX = pOrdent[i];
					POS = i;
				}
			}
			System.out.println(noms.get(POS) + "                  " + pGuanya.get(POS) + "                       "
					+ pPerduda.get(POS));
			pOrdent[POS] = -1;
		}
	}

	/**
	 * Constructor de tipo String updatePlayer() se encarga de recoger la respuesta
	 * del booleano y en caso de verdadero, le asigna a ese jugador una victoria o
	 * una derrota
	 * @param player Obtiene el nombre del jugador
	 * @param guanya Obtiene si el jugador ha ganado o no
	 */
	public static void updatePlayer(String player, boolean guanya) {
		if (noms.contains(player)) {
			int pos = noms.indexOf(player);
			if (guanya == true) {
				pGuanya.set(pos, pGuanya.get(pos) + 1);
			} else {
				pPerduda.set(pos, pPerduda.get(pos) + 1);
			}
		}
	}

}
