package buscaMinas;
import java.util.*;
/**
 * <h2>Clase Jugar contiene todas las funciones necesarias para poder jugar</h2>
 *
 * @version 1-2022
 * @author isanchez
 */


public class Jugar {
	static Scanner src = new Scanner(System.in);
	/**
	 * Int est�tico en el que se guarda el n�mero de filas que utilizaremos en el
	 * tablero
	 */
	static int files;
	/**
	 * Int est�tico en el que se guarda el n�mero de columnas que utilizaremos en el
	 * tablero
	 */
	static int columnes;
	/**
	 * Int est�tico en el que se guarda el n�mero de minas que utilizaremos en el
	 * tablero
	 */
	static int mines;
	/**
	 * Int est�tico en el que se guarda el n�mero de minas que utilizaremos en el
	 * tablero
	 */
	static final int MAX = 50;

	/**
	 * Constructor encargado de inicializar las variables y llamar a los
	 * constructores correspondientes necesario
	 * 
	 * @param player Nombre del jugador
	 */
	public static void jugar(String player) {
		// Variables necess�ries
		Casella[][] taulell; // Taulell que mostrarem, inicialment tot a '?'
		Casella[][] secret; // Taulell n�meros i mines, i espais en blanc
		int fila, column; // fila i columna seleccionada per l'usuari
		int caselles; // Indica el nombre de caselles que resten tapades
		boolean esMina;
		boolean primeraVegada;
		boolean guanya;

		// Inicialitzem variables
		esMina = false;
		primeraVegada = true;
		taulell = inicialitzaTaulell(files, columnes);
		secret = posarMines(mines, files, columnes);
		caselles = 0;
		mostrar(secret, files, columnes);

		do {
			mostrar(taulell, files, columnes);
			System.out.println();
			// mostrar(secret, files, columnes);
			esMina = tirada(taulell, secret, files, columnes);
			caselles = comprovar(taulell, files, columnes);
		} while (!esMina && caselles > mines);
		guanya = comprovarResultats(esMina, player);
		Jugador.updatePlayer(player, guanya);

	}

	/**
	 * Constructor encargado de mostrar por pantalla si la casilla que hab�as
	 * seleccionado era bomba o no
	 * 
	 * @param player Nombre del jugador
	 * @param esMina Indica si hab�a una mina en esa posici�n
	 * @return check Para indicar si gan�, contin�a o perdi�.
	 */
	public static boolean comprovarResultats(boolean esMina, String player) {
		// TODO Auto-generated method stub
		boolean check;
		if (esMina == true) {
			check = false;
			System.out.println("Vaja " + player + " has fet explotar una bomba, m�s sort la pr�xima vegada");
		} else {
			check = true;
			System.out.println("Enhorabona " + player + " has guanyat la partida!");
		}
		return check;
	}

	/**
	 * Constructor encargado de verificar si la casilla seleccionada est� tapada
	 * 
	 * @param taulell   Tama�o del tablero con el que jugamos
	 * @param files2    Indica el n�mero de filas que contiene el tablero
	 * @param columnes2 Indica el n�mero de columnas que contiene el tablero
	 * @return cont Devuelve el n�mero de casillas que ha destapado
	 */
	public static int comprovar(Casella[][] taulell, int files2, int columnes2) {
		// TODO Auto-generated method stub
		int cont = 0;
		for (int i = 0; i < files2; i++) {
			for (int j = 0; j < columnes2; j++) {
				if (taulell[i][j] == Casella.Tapada) {
					cont++;
				}
			}
		}
		return cont;
	}

	/**
	 * Constructor que prepara la jugada, i reempla�a el valor d'un taulell per el
	 * taulell secret
	 * 
	 * @param taulell   Taulell principal en blanc
	 * @param secret    Taulell que cont� les mines, i n�meros
	 * @param files2    N�mero de files que cont� el taulell principal
	 * @param columnes2 N�mero de columnes que cont� el taulell principal
	 * @return esMina Retorna si la casella seleccionada era una mina o no
	 */
	public static boolean tirada(Casella[][] taulell, Casella[][] secret, int files2, int columnes2) {
		// TODO Auto-generated method stub
		int fila;
		int columna;
		boolean esMina = false;
		int cont;
		System.out.print("Escull la fila que vols destapar: ");
		fila = obtenirValor(files2) - 1;
		System.out.print("\nEscull la columna de la fila que vols destapar: ");
		columna = obtenirValor(columnes2) - 1;
		if (secret[fila][columna] != Casella.Mina) {
			cont = destapar(taulell, secret, fila, columna, files2, columnes2);
			esMina = false;
		} else {
			esMina = true;
		}
		return esMina;

	}

	/**
	 * Constructor que s'encarrega de verificar el contingut de la casella
	 * seleccionada i utilitza recusivitat per destapar les altres
	 * 
	 * @param taulell   Taulell principal
	 * @param secret    Taulell ocult que cont� les mines i n�meros
	 * @param fila      Fila seleccionada per l'usuari
	 * @param columna   Columna seleccionada per l'usuari
	 * @param files2    N�mero de files que cont� el taulell principal
	 * @param columnes2 N�mero de columnes que cont� el taulell principal
	 * @return N�mero de caselles que s'han destapat per restar del total
	 */
	public static int destapar(Casella[][] taulell, Casella[][] secret, int fila, int columna, int files2,
			int columnes2) {
		// TODO Auto-generated method stub
		int cont = 0;
		if (fila >= 0 && fila < files && columna >= 0 && columna < columnes2
				&& taulell[fila][columna] == Casella.Tapada) {
			taulell[fila][columna] = secret[fila][columna];
			cont++;
			if (secret[fila][columna] == Casella.Blanc) {
				for (int i = -1; i <= 1; i++) {
					for (int j = -1; j <= 1; j++) {
						cont = cont + destapar(taulell, secret, fila + i, columna + j, files2, columnes2);
					}
				}
			}
		}
		return cont;
	}

	/**
	 * Constructor que s'encarrega de reempla�ar el valor de la casella en format
	 * enum, a int
	 * 
	 * @param taulell   Taulell principal
	 * @param files2    N�mero de files que cont� el taulell principal
	 * @param columnes2 N�mero de columnes que cont� el taulell principal
	 */
	public static void mostrar(Casella[][] taulell, int files2, int columnes2) {
		// TODO Auto-generated method stub
		System.out.print("    ");
		for (int i = 0; i < columnes2; i++) {
			System.out.printf("%4d", (i + 1));
		}
		System.out.println();
		for (int i = 0; i < files2; i++) {
			System.out.printf("%4d", (i + 1));
			for (int j = 0; j < columnes2; j++) {
				switch (taulell[i][j]) {
				case Blanc:
					System.out.printf("%4c", ' ');
					break;
				case Tapada:
					System.out.printf("%4c", '?');
					break;
				case Bandereta:
					System.out.printf("%4c", '!');
					break;
				case Mina:
					System.out.printf("%4c", '*');
					break;
				case Un:
					System.out.printf("%4c", '1');
					break;
				case Dos:
					System.out.printf("%4c", '2');
					break;
				case Tres:
					System.out.printf("%4c", '3');
					break;
				case Quatre:
					System.out.printf("%4c", '4');
					break;
				case Cinc:
					System.out.printf("%4c", '5');
					break;
				case Sis:
					System.out.printf("%4c", '6');
					break;
				case Set:
					System.out.printf("%4c", '7');
					break;
				case Vuit:
					System.out.printf("%4c", '8');
					break;
				}
			}
			System.out.println();
		}

	}

	/**
	 * Constructor que s'encarrega de fer recursivitat junt uns altres m�todes per
	 * poder complir la funci� de posar mines al taulell
	 * 
	 * @param mines2    N�mero de mines seleccionades per l'ususari
	 * @param files2    N�mero de files seleccionades per l'usuari
	 * @param columnes2 N�mero de columnes seleccionades per l'usuari
	 * @return Omplim el taulell secret
	 */
	public static Casella[][] posarMines(int mines2, int files2, int columnes2) {
		// TODO Auto-generated method stub
		Casella[][] secret = new Casella[files2][columnes2];
		colocaMines(secret, files, columnes, mines);
		colocaNumeros(secret, files, columnes);
		return secret;
	}

	/**
	 * Constructor que s'encarrega de col�locar els n�meros acorde amb la posici� i
	 * distancia d'una mina
	 * 
	 * @param secret    Taulell secret on hi han les mines, i n�meros
	 * @param files2    N�mero de files que cont� el taulell secret
	 * @param columnes2 N�mero de columnes que cont� el taulell secret
	 */
	private static void colocaNumeros(Casella[][] secret, int files2, int columnes2) {
		// TODO Auto-generated method stub
		int cont = 0;
		for (int i = 0; i < files2; i++) {
			for (int j = 0; j < columnes2; j++) {
				if (secret[i][j] == Casella.Blanc) {
					if (i == 0) {
						if (j == 0) {
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
							if (secret[i + 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
						} else if (j == secret[i].length - 1) {
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
							if (secret[i + 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
						} else {
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
							if (secret[i + 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
						}
					} else if (i == secret.length - 1) {
						if (j == 0) {
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
						} else if (j == secret[i].length - 1) {
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
						} else {
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
							if (secret[i - 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
						}
					} else {
						if (j == 0) {
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
						} else if (j == secret[i].length - 1) {
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
						} else {
							if (secret[i - 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i - 1][j] == Casella.Mina)
								cont++;
							if (secret[i - 1][j + 1] == Casella.Mina)
								cont++;
							if (secret[i][j - 1] == Casella.Mina)
								cont++;
							if (secret[i][j + 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j - 1] == Casella.Mina)
								cont++;
							if (secret[i + 1][j] == Casella.Mina)
								cont++;
							if (secret[i + 1][j + 1] == Casella.Mina)
								cont++;
						}
					}
				}

				if (cont == 1)
					secret[i][j] = Casella.Un;
				else if (cont == 2)
					secret[i][j] = Casella.Dos;
				else if (cont == 3)
					secret[i][j] = Casella.Tres;
				else if (cont == 4)
					secret[i][j] = Casella.Quatre;
				else if (cont == 5)
					secret[i][j] = Casella.Cinc;
				else if (cont == 6)
					secret[i][j] = Casella.Sis;
				else if (cont == 7)
					secret[i][j] = Casella.Set;
				else if (cont == 8)
					secret[i][j] = Casella.Vuit;
				cont = 0;
			}
		}
	}

	/**
	 * Constructor que s'encarrega de manera random de col�locar a la casella blanca
	 * mines en tot cas
	 * 
	 * @param secret Taulell secret que cont� les mines, n�meros i caselles en blanc
	 * @param files2 N�mero de files seleccionades per l'usuari
	 * @param columnes2 N�mero de columnes seleccionades per l'usuari
	 * @param mines2 N�mero de mines seleccionades per l'usuari
	 */
	public static void colocaMines(Casella[][] secret, int files2, int columnes2, int mines2) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		for (int i = 0; i < files2; i++) {
			for (int j = 0; j < columnes2; j++) {
				secret[i][j] = Casella.Blanc;
			}
		}
		do {
			int valorx = rnd.nextInt(files2);
			int valory = rnd.nextInt(columnes2);
			if (secret[valorx][valory] == Casella.Blanc) {
				secret[valorx][valory] = Casella.Mina;
				mines2--;
			}
		} while (mines2 > 0);
	}

	/**
	 * Constructor que s'encarrega d'assignar-li el tamany establert per l'usuari i
	 * possar les caselles en blanc
	 * 
	 * @param files2   N�mero de files seleccionades per l'usuari
	 * @param columnes N�mero de columnes seleccionades per l'usuari
	 * @return El taulell complert i inicializat
	 */
	public static Casella[][] inicialitzaTaulell(int files2, int columnes) {
		Casella[][] taulell = new Casella[files2][columnes];
		for (int i = 0; i < files2; i++) {
			for (int j = 0; j < columnes; j++) {
				taulell[i][j] = Casella.Tapada;
			}
		}
		return taulell;
	}

	/**
	 * Constructor que s'encarrega de seleccionar el tipus de dificultat que voldr�s
	 * per aix� establir uns par�metres per defecte o personalitzats
	 */
	public static void selectDificult() {
		// TODO Auto-generated method stub
		System.out.println("Definir nivell de dificultat");
		System.out.println(
				"(1) - Nivell principiant: 8 x 8 caselles i 10 mines.\n(2) - Nivell intermedi: 16 x 16 caselles i 40 mines.\n(3) - Nivell expert: 16 x  30 caselles i 99 mines.");
		System.out.print("Escriu el nivell de dificultat que vols utilitzar: ");
		int dificultat = src.nextInt();
		if (dificultat == 1) {
			files = 8;
			columnes = 8;
			mines = 10;
		} else if (dificultat == 2) {
			files = 16;
			columnes = 16;
			mines = 40;
		} else if (dificultat == 3) {
			files = 16;
			columnes = 30;
			mines = 99;
		} else if (dificultat == 4) {
			System.out.println("Introdueix n�mero de files que vols al taulell: ");
			files = obtenirValor(MAX);
			System.out.print("\nIntrodueix n�mero de columnes que vols al taulell: ");
			columnes = obtenirValor(MAX);
			System.out.print("\nIntrodueix n�mero de mines que vols al taulell: ");
			mines = obtenirValor((int) (files * columnes * 0.20));
		}
	}

	/**
	 * Constructor que s'encarrega d'obtenir els valors del taulell personalitzat i comprovar que siguin correctes
	 * @param max2 Valor m�xim que �s pot establir
	 * @return Retorna el tamany seleccionat per l'usuari validat
	 */
	public static int obtenirValor(int max2) {
		// TODO Auto-generated method stub
		boolean check = false;
		int v = 0;
		do {
			try {
				v = src.nextInt();
				if (v > 0 && v <= max2) {
					check = true;
				}
			} catch (Exception e) {
				System.out.println("Cal introduir un numero entre 0 y" + max2);
				src.nextLine();
			}

		} while (check == false);

		return v;
	}
}