package buscaMinas;
/**
 * <h2>Constructor de enum con los valores que se introducirán en la matriz y posteriormente serán reemplazados</h2>
 *
 * @version 1-2022
 * @author isanchez
 */
public enum Casella {
	Tapada, Blanc, Bandereta, Mina, Un, Dos, Tres, Quatre, Cinc, Sis, Set, Vuit

}
