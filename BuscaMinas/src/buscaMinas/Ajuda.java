package buscaMinas;
/**
 * <h2>Clase Ayuda se encarga de mostrarte una peque�a gu�a con el funcionamiento del juego</h2>
 *
 * @version 1-2022
 * @author isanchez
 */

public class Ajuda {

	public static void ajuda() {
		// Llegeix una opci� del men�, la valida i la retorna al main()
		System.out.println("El joc consisteix a netejar totes les caselles d'una pantalla que no amaguin una mina.\r\n"
				+ "\r\n"
				+ "Algunes caselles tenen un n�mero. Aquest n�mero indica les mines que sumen totes les caselles circumdants. "
				+ "Aix�, si una casella t� el n�mero 3 vol dir que de les vuit caselles que l'envolten (excepte si es troba a una vora o una cantonada) "
				+ "n'hi ha 3 amb mines i 5 sense. Si es descobreix una casella sense n�mero ens indica que cap de les caselles ve�nes t� mina i aquestes es "
				+ "descobreixen autom�ticament.\r\n" + "\r\n"
				+ "Si es descobreix una casella amb mina es perd la partida.\r\n"
				+ "Es pot posar una marca a les caselles en qu� el jugador pensa que hi ha mina per tal d'ajudar-nos a descobrir les que s'hi troben a prop.\r\n"
				+ "El joc tamb� t� un sistema de r�cords per a cadascun dels tres nivells en els quals s'indica el menor temps necessitat per concloure el joc. "
				+ "Els nivells s�n (per a les noves versions).\r\n" + "\r\n"
				+ "Nivell principiant: 9 � 9 caselles i 10 mines.\r\n"
				+ "Nivell intermedi: 16 � 16 caselles i 40 mines.\r\n" + "Nivell expert: 16 � 30 caselles i 99 mines.");
	}
}
