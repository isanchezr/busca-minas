package buscaMinas;
import java.util.*;
/**
 * <h2>Clase Programa es la clase principal donde se situa el men� del juego y llamaremos a las funciones correspondientes</h2>
 *
 * @version 1-2022
 * @author isanchez
 */

public class Programa {

	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Atributo opcio => Int que define mediante la funci�n <i>mostrarMenu</i> la
		 * opci�n del men� que quieres escoger
		 */
		int opcio = 0;
		/**
		 * Atributo player => String que define mediante la funci�n <b>nombre de la
		 * clase Jugador</b> el nombre del jugador
		 */
		String player = null;
		/**
		 * Atributo assignt => Define si ya has definido el nombre de tu usuario y la
		 * dificultad del juego
		 */
		boolean assignt = false;
		do {

			opcio = mostrarMenu(); // m�tode definit per nosaltres. Retorna l'opci� de men� escollida per l'usuari

			switch (opcio) {
			case 0:
				System.out.println("Moltes gr�cies per jugar\n\nAdeu!!");
				break;
			case 1:
				Ajuda.ajuda();	//Ajuda
				break;
			case 2:
				System.out.println("\n_______\nOPCIONS\n-------\n\n");
				player = Jugador.nombre(); // Definim nom del jugador
				Jugar.selectDificult(); // Definim la dificultat del joc
				assignt = true;
				break;
			case 3:
				if (assignt == true) {
					Jugar.jugar(player);
					assignt = false;
				} else {
					System.out.println("Error, cal definir primer el jugador");
				}
				break;
			case 4:
				Jugador.showPlayer(player);
				// Jugador.showPlayer(player2);
				break;
			default:
				System.out.print("Opci� erronia");
			}
		} while (opcio != 0);

	}

	/**
	 * Constructor mostrarMenu Muestra por pantalla las opciones de las que dispone
	 * el men� del juego y devuelve el valor a la variable <i>opcio</i>
	 * @return opcio i retorna el valor corresponent a l'opci� que has escollit del men�
	 */

	public static int mostrarMenu() {
		// Llegeix una opci� del men�, la valida i la retorna al main()
		int opcio;
		boolean correcte = false;

		do {
			System.out.println("\nBUSCAMINAS:\n");
			System.out.println("1 - Obtenir ajuda");
			System.out.println("2 - Opcions");
			System.out.println("3 - Jugar");
			System.out.println("4 - Ranking");
			System.out.println("0 - Sortir");
			System.out.print("\n\nTria una opci�: ");

			opcio = src.nextInt();
			if (opcio == 0 || opcio == 1 || opcio == 2 || opcio == 3 || opcio == 4)
				correcte = true;
			else
				System.out.println("Entrada incorrecta. Torna-ho a provar");
		} while (!correcte);
		System.out.println("El m�tode retorna: " + opcio);
		return opcio; // retorna el valor guardat en opcio a qui ho ha cridat
	}
}
